//
//  pokeDexViewController.swift
//  MarvelApp
//
//  Created by andrea villacis on 29/11/17.
//  Copyright © 2017 andrea villacis. All rights reserved.
//

import UIKit

class pokeDexViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate{
    
    
    
    @IBOutlet weak var pokedexTableView: UITableView!
    
    var pokemonArray:  [Pokemon] =  []
    var pokemonIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
    
    let service = PokemosService()
    service.delegate = self
    service.donwloadPokemons()
    }
    
    
    func get20FirstPokemons(pokemons: [Pokemon]) {
        pokemonArray = pokemons
        pokedexTableView.reloadData()//para q muestre en la tabla 
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch section {
//        case 0:
//            return 5
//        default:
//            return 10
        //         }
        return pokemonArray.count
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
      
        cell.fillData(pokemon: pokemonArray[indexPath.row])
      
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender:Any?){
        let pokemonDetail = segue.destination as! detailViewController
        pokemonDetail.pokemon = pokemonArray[pokemonIndex]
    
    }
    
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        pokemonIndex = indexPath.row
        return indexPath
    }
    
    
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "section 1"
        default:
            return "section 2"
        }
    }

}

