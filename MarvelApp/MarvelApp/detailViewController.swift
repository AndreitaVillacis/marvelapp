//
//  detailViewController.swift
//  MarvelApp
//
//  Created by andrea villacis on 3/1/18.
//  Copyright © 2018 andrea villacis. All rights reserved.
//

import UIKit

class detailViewController: UIViewController {

    var pokemon:Pokemon?
    
    
    @IBOutlet weak var pokemonImageVIew: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(pokemon?.name ?? "")
        self.title = pokemon?.name
        
        let pkService = PokemosService()
        
        pkService.getPokemonImage(id: (pokemon?.id)!){(pkImage) in
        self.pokemonImageVIew.image=pkImage
        }
    }
    
}
